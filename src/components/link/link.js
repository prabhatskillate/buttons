// import "./global-styles.less";

// .btn {
//     height: 34px;
//     padding: auto 16px;
// }


import * as theme from '../global-styles.js';
import { css } from 'react-emotion';

//import {fontColor, fontSize} from '../global-styles.js';


export const alignContainer = css`
  display:flex;
  align-content: space-between;
  flex-grow: 1;
`

export const alignIcon = css`
  display:flex;
  align-items: center;
`

export const alignText = css`
  display:flex;
  align-items: center;
`
export const rowReverse = css`
  flex-direction:row-reverse;
`
export const buttonGrow = css`
  flex-grow:1;
`
export const paddingRight = css`
padding-right:8px;
`

export const anchor = css`

  text-decoration: none;
  display:flex;
  font-family: ${theme.fontFamily.text};
  cursor: pointer;
  font-size: ${theme.fontSize.regular};
  font-weight: ${theme.fontWeight.regular};
  color: ${theme.fontColor.primaryLink};
  &:hover {
  text-decoration: underline;
}
`
export const anchordefault = css`
color: ${theme.fontColor.primaryLink}; 

`
export const anchorsecondary = css`
color: ${theme.fontColor.secondaryLink} ;

`
export const anchorexternal = css`
color: ${theme.fontColor.externalLink} ;

`
export const anchordisabled = css`
color:${theme.fontColor.veryLight};
cursor: not-allowed;

`