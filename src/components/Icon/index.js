import React from 'react';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheckSquare, faCoffee, faArrowRight, faArrowLeft, faEnvelope, faChevronRight } from '@fortawesome/pro-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

library.add(faCheckSquare, faCoffee, faArrowRight, faArrowLeft, faEnvelope, faChevronRight)



const Icon = ({
  className = '',
  name,
  size = 'inherit',
  color = 'inherit',
  cursor = 'inherit',
  ...rest
}) => (

  <FontAwesomeIcon icon={name} 
     />


);

export default Icon;
