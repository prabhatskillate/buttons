import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

var fontFile = document.createElement('link');
fontFile.setAttribute('rel', 'stylesheet');
fontFile.setAttribute('type', 'text/css');
fontFile.setAttribute('href', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i|Titillium+Web:400,700');
document.head.appendChild(fontFile);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
