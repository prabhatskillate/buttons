import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { css,  keyframes  }  from 'react-emotion';

// import './loader.css'

const strand_shrink = keyframes`
{
  0% {
    opacity: 1;
    stroke-dasharray: 0 20;
    stroke-dashoffset: -10;
  }
  100% {
    opacity: 1;
    stroke-dasharray: 20 20;
    stroke-dashoffset: 0;
  }
`
const strand_shrink_2 = keyframes`

  from {
    opacity: 1;
    stroke-dasharray: 0 20;
    stroke-dashoffset: -20;
  }
  to {
    opacity: 1;
    stroke-dasharray: 20 20;
    stroke-dashoffset: 0;
  }
`
const draw = keyframes`

  from {
    opacity: 1;
    stroke-dasharray: 150 150;
    stroke-dashoffset: 150;
  }
  to {
    opacity: 1;
    stroke-dasharray: 150 150;
    stroke-dashoffset: 0;
  }
`

const animate1 = css`

  animation: ${strand_shrink} 5s ease forwards;
  transform: scale(0.5, 0.5);
`

const animate2 = css`
  fill-opacity: 0;
  opacity: 0;
  animation: ${strand_shrink_2} 5s ease;

  animation-iteration-count: infinite;
  transform: scale(0.5, 0.5);
`

const animate3 = css`
  animation: draw 5s linear;
  transform: scale(0.5, 0.5);
`

const animate4 = css`
  fill-opacity: 0;
  opacity: 0;
  animation: ${draw} 5s linear forwards;

  animation-iteration-count: infinite;
  transform: scale(0.5, 0.5);
`







const propTypes = {
  
  className: PropTypes.string,
};

const defaultProps = {

};
let k = -1;
class Loader extends React.Component {
  state = {
    front2: '#D9DCE2',
    front1: '#2A5CB2',
    back2: '#D9DCE2',
    back1: '#9CA6BA',
    strand2: '#D9DCE2',
    strand1: '#9CA6BA'
  }

  f1 = ({front1, front2, back1, back2, strand1, strand2}) => ({
   
    front1 : front2,
    front2: "#2A5CB2",
    back1: back2,
    back2: '#9CA6BA',
    strand1: strand2 ,
    strand2: '#9CA6BA',
  });

  f2 = ({front1, front2, back1, back2, strand1, strand2}) => ({
    front1 : front2,
    front2: "#84BB23",
    back1: back2,
    back2: '#2A5CB2',
    strand1: strand2 ,
    strand2: '#2A5CB2',
  });

  f3 = ({front1, front2, back1, back2, strand1, strand2}) => ({
    front1 : front2,
    front2: "#D9DCE2",
    back1: back2,
    back2: '#D9DCE2',
    strand1: strand2 ,
    strand2: '#D9DCE2',
  });


  componentDidMount =() =>{
    //  setTimeout(this.animate,1000);
    
    let self = this;
    let funArr = [self.f1, self.f2, self.f3];
    setInterval(() => {
      k += 1;
      k = k % 3;
      self.setState(funArr[k])
    }, 5500);
   
    // for (let i = 0; i < funArr.length; i++) {

    //     let interval = 1000 * (i + 1);
    //     let self = this;
    //     (function (k,interval2) {
    //         setInterval(function(){
    //           self.setState(funArr[k])
    //         }, 1000);
    //     })(i,interval);


        

    //   }



//     var self = this;
//     window.setInterval(() => self.setState(
// this.f1, this.f2, this.f3
//     ), 1000);

    // window.setInterval(() => self.setState(
    //   ({front1, front2, back1, back2, strand1, strand2}) => ({
    //     front1: front1 === 'red' ? 'green' : 'red',
    //     front2: front2 === 'red' ? 'green' : 'red',
    //     back1: back1 === 'green' ? 'red' : 'green',
    //     back2: back2 === 'red' ? 'green' : 'red',
    //     strand1: strand1 === 'green' ? 'red' : 'green',
    //     strand2: strand2 === 'red' ? 'green' : 'red',
    //   })
    // ), 1000);


    
  
  //  let animate = ()=> {
    
  //     var self = this;
  //     self.setState(
  //       )
  //     ;
    
  //     setTimeout(() => {
  //       self.setState(

  //       );
  //     }, 1000);
    
  //     setTimeout(() => {
  //       self.setState(

  //       );
  //     }, 2000);
    
  //     setTimeout(this.animate, 3000);
  //   }

  }

  onClick =(e) =>{
    if (this.props.disabled) {
      e.preventDefault();
      return;
    }

    if (this.props.onClick) {
      this.props.onClick(e);
    }
  }

  render() {
    let {

      className,

      ...attributes
    } = this.props;



    let classes = classNames(
      className
    );






      return ( <div { ...attributes} className = {classes} style = {{fontWeight : 600}} onClick = {this.onClick}>
        <div className="svg-container">
  <svg width="78" height="100" xmlns="http://www.w3.org/2000/svg" version="1.1" id="SVGRoot" className="center">
   <g transform="translate(10.000000, 10.000000)" strokeWidth="6">
    <path id="back" 
    className={animate3}
          d="M0,0 L27.2861819,0 C41.4877862,-2.60879238e-15 53.8732222,9.65017907 57.3453825,23.4207878 L59.6546136,32.5792122 C63.126774,46.3498209 75.51221,56 89.7138143,56 L117,56"
          stroke={this.state.back1}
          strokeLinecap="round"  
          fill="transparent" />
          <g id="strands" stroke={this.state.strand1}>
    <path id="strand-right" className={`strand_shrink ${animate1}`} d="M10,18.3125 L10,38.3125"  strokeLinecap="round"  fill="transparent" />
    <path id="strand-right-mid" className={`strand_shrink ${animate1}`} d="M29,18.3125 L29,38.3125" strokeLinecap="round"  fill="transparent" />
    <path id="strand-left-mid" className={`strand_shrink ${animate1}`} d="M88,18.3125 L88,38.3125"  strokeLinecap="round"  fill="transparent" />
    <path id="strand-left" className={`strand_shrink ${animate1}`} d="M108,18.3125 L108,38.3125" strokeLinecap="round"  fill="transparent" />
          </g>
    <path id="front" 
    className={animate3}
          d="M0,56 L27.2841456,56 C41.4850709,56 53.8700972,46.350725 57.3429008,32.5809786 L59.6535953,23.4190214 C63.1263989,9.64927503 75.5114252,1.07949094e-13 89.7123505,6.75015599e-14 L117,0"
          stroke={this.state.front1}       
          strokeLinecap="round"  
          fill="transparent" />
    </g>
    
    <g transform="translate(10.000000, 10.000000)" strokeWidth="6" id="group2">
      <path id="back2" className={animate4}
            d="M0,0 L27.2861819,0 C41.4877862,-2.60879238e-15 53.8732222,9.65017907 57.3453825,23.4207878 L59.6546136,32.5792122 C63.126774,46.3498209 75.51221,56 89.7138143,56 L117,56"
            stroke={this.state.back2}
            strokeLinecap="round"  
            fill="transparent" 
           opacity ="0"/>
            <g id="strands" stroke={this.state.strand2}>
      <path id="strand-right" className={`strand_shrink_2 ${animate2}`} d="M10,18.3125 L10,38.3125"  strokeLinecap="round"  fill="transparent" opacity ="0" />
      <path id="strand-right-mid" className={`strand_shrink_2 ${animate2}`} d="M29,18.3125 L29,38.3125" strokeLinecap="round"  fill="transparent" opacity ="0"/>
      <path id="strand-left-mid" className={`strand_shrink_2 ${animate2}`} d="M88,18.3125 L88,38.3125"  strokeLinecap="round"  fill="transparent" opacity ="0"/>
      <path id="strand-left" className={`strand_shrink_2 ${animate2}`} d="M108,18.3125 L108,38.3125" strokeLinecap="round"  fill="transparent" opacity ="0"/>
            </g>
      <path id="front2" className={animate4}
            d="M0,56 L27.2841456,56 C41.4850709,56 53.8700972,46.350725 57.3429008,32.5809786 L59.6535953,23.4190214 C63.1263989,9.64927503 75.5114252,1.07949094e-13 89.7123505,6.75015599e-14 L117,0"
            stroke={this.state.front2}     
            strokeLinecap="round"  
            fill="transparent" 
            opacity ="0"
            />
      </g>
    
    
  </svg>
</div>

        </div>
        
      );
    }
  }


Loader.propTypes = propTypes;
Loader.defaultProps = defaultProps;

export default Loader;