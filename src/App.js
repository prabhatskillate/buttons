import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from '../src/components/buttons';
import Link from '../src/components/link';
import Loader from '../src/components/loader';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div className="middle">
        <Button color="blue" width="100" iconName="chevron-right" iconAlign="right">
          link
        </Button>
        </div>
        <div className="middle">
        <Link   href="www.skillate.com" iconAlign="right" anchorType="external" fontWeight="">
          Hello World 2
        </Link>
        </div>
        <div className="middle">
        <Button color="blue" width="200">
        vjgkbj
        </Button>
        </div>
        <div className="middle">
        <Button color="regular" textColor="green" iconName="coffee" href="www.skillate.com">
          Hello World
        </Button>
        </div>
        <div className="middle">
        <Loader>
          Hello World
        </Loader>
        </div>
        <div className="middle">
        <Button color="subtle" textColor="red" >
          Hello World
        </Button>
        </div>
      </div>
    );
  }
}

export default App;
