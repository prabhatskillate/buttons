import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Icon from '../Icon';


import * as buttonStyle from './button.js';


const propTypes = {

  color: PropTypes.string,
  textColor: PropTypes.string,
  iconAlign: PropTypes.string,
  iconName: PropTypes.string,
  anchorType:PropTypes.string,
  width:PropTypes.string,
  onClick: PropTypes.func,
  innerRef: PropTypes.oneOfType([PropTypes.object, PropTypes.func, PropTypes.string]),
  className: PropTypes.string,
};

const defaultProps = {
  color: 'regular',
  
  iconAlign: "left",
  width:"auto",

  
};

class Button extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    if (this.props.disabled) {
      e.preventDefault();
      return;
    }

    if (this.props.onClick) {
      this.props.onClick(e);
    }
  }

  render() {
    let {
      color,
      textColor,
      iconAlign,
      iconName,
      anchorType,
      width,
      tag: Tag,
      innerRef,
      className,

      ...attributes
    } = this.props;



    let classes = classNames(
      className,
      buttonStyle.btn,
      {   
        [buttonStyle.btn_blue]: color === "blue",
        [buttonStyle.btn_regular]: color === "regular",
        [buttonStyle.btn_subtle]: color === "subtle",
        [buttonStyle.btn_skeleton]: color === "skeleton",
        [buttonStyle.red_text]: textColor === "red",
        [buttonStyle.green_text]: textColor === "green",
      },
    );

    let alignContainer = classNames(
      buttonStyle.alignContainer,
      {
      [buttonStyle.rowReverse]: iconAlign === "right",
      }
    );


    let alignText = classNames(
      buttonStyle.alignText, 
      { 
        [buttonStyle.column_btn]: (!iconName),
        [buttonStyle.paddingRight]: (iconAlign === "right" && iconName  && this.props.children),
        [buttonStyle.buttonGrow]: (iconAlign === "right" && iconName  && this.props.children)
        
      }
    );

    let alignIcon = classNames(
      buttonStyle.alignIcon,
      
      { [buttonStyle.column_btn]: (!this.props.children),
        [buttonStyle.paddingRight]: (iconAlign === "left" && iconName && this.props.children),
        [buttonStyle.buttonGrow]: (iconAlign === "left" && iconName && this.props.children),
      }
    );


  console.log(iconName);




      return ( <button  { ...attributes
        }
        className = {
          classes
        }
        style = {{width: width + 'px'}}

        ref = {
          innerRef
        }
        onClick = {
          this.onClick
        }
        >
        <div className={alignContainer}>
        <div className={alignIcon}><Icon name={['far', iconName]}/></div>
        <div className={alignText} >{attributes.children}</div>
        </div>
        </button>
        
      );
    }
  }


Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;