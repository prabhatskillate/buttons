import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Icon from '../Icon';


import * as buttonStyle from './link.js';
import { cx } from 'emotion';


const propTypes = {


  iconAlign: PropTypes.string,
  iconName: PropTypes.string,
  anchorType:PropTypes.string,
  fontWeight:PropTypes.string,
  onClick: PropTypes.func,
  innerRef: PropTypes.oneOfType([PropTypes.object, PropTypes.func, PropTypes.string]),
  className: PropTypes.string,
};

const defaultProps = {
  fontWeight: "regular",
  iconAlign: "left"
};

class Link extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    if (this.props.disabled) {
      e.preventDefault();
      return;
    }

    if (this.props.onClick) {
      this.props.onClick(e);
    }
  }

  render() {
    let {

      iconAlign,
      iconName,
      anchorType,
      innerRef,
      className,
      fontWeight,
      ...attributes
    } = this.props;



    let classes = cx(
      className,buttonStyle.anchor,
      {
        [buttonStyle.anchorsecondary]: anchorType === "secondary",
        [buttonStyle.anchorexternal]: anchorType === "external",
        [buttonStyle.anchordisabled]: anchorType === "disabled",
      }
    );

    let alignContainer = classNames(
      buttonStyle.alignContainer,
      {
      [buttonStyle.rowReverse]: iconAlign === "right",
      }
    );

    let alignIcon = classNames(
      buttonStyle.alignIcon,
      {
        [buttonStyle.paddingRight]: (iconAlign === "left" && iconName && this.props.children),
        [buttonStyle.buttonGrow]: (iconAlign === "left" && iconName && this.props.children),
      }
    );

    let alignText = classNames(
      buttonStyle.alignText,
      {
        [buttonStyle.paddingRight]: (iconAlign === "right" && iconName && this.props.children),
        [buttonStyle.buttonGrow]: (iconAlign === "right" && iconName && this.props.children),
      }
    );






      return ( <a { ...attributes
        }
        className = {
          classes
        }
        style = {fontWeight === "bold" ? {fontWeight : 600} : {fontWeight : 400}}
        ref = {
          innerRef
        }
        onClick = {
          this.onClick
        }
        >
        <div className={alignContainer}>
        <div className={alignIcon}><Icon name={['far', iconName]}/></div>
        <div className={alignText} >{attributes.children}</div>
        </div>
        </a>
        
      );
    }
  }


Link.propTypes = propTypes;
Link.defaultProps = defaultProps;

export default Link;