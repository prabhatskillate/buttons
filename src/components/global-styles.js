



export var fontFamily = {text :"'Source Sans Pro', sans-serif", numbers :" 'Titillium Web', sans-serif"}
export var fontSize = {small:"12px", regular:"14px", Large:"18px", Header:"21px"}
export var fontWeight = {regular:400, bold:600, numberBold:700}
export var fontColor = {dark:"#00153a", light:"#56647c", veryLight:"#9ca6ba", white:"#ffffff", darkBackground:"#D4D8E0", primaryLink:"#2a5cb2", secondaryLink:"#56647C", externalLink:"#09a1e3"}

export var colors = {white:"#ffffff",defaultblue: "#2a5cb2", hoverBlue:"#165be5", blue:"#09a1e3", green:"#1baa96", red:"#e5432d", orange:"#ff6600", yellow:"#fad223"}

export var backgroundColors = {white: "#ffffff", veryLightGrey:"#f8f8f8", lightGrey:"#f1f1f1", defaultblue:"#dfe6f3", green:"#ddf2ef", blue:"#daf0fa", yellow:"#fef8de", red:"#fbe2df", orange:"#ffe8d9", dark:"#3a3c3e" }

export var shadow = { active:"inset 0 0 4px 0 rgba(0, 0, 0, 0.2)", popout:"0 2px 6px 0 rgba(0, 21, 58, 0.2)"}

export var borderColors = {default:"#d9dce2", white:"#ffffff", active:"#B4B9C4", borderbottom: "1px solid rgba(0,0,0,0.3)"}

export var radius= {button:"2px", base:"2px"}

export var dimensions = {buttonHeight:"34px", inputHeight:"34px", verticalPadding:"4px", HorizontalPadding:"8"} //use padding with multipliers like 1x 2x etc
