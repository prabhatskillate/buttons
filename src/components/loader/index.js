import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { css,  keyframes  }  from 'react-emotion';
import { Samy, SvgProxy } from 'react-samy-svg';






const propTypes = {
  
  className: PropTypes.string,
};

const defaultProps = {

};
let k = -1;
let combination = 0;
class Loader extends React.Component {




  state = {
    front2: '#D9DCE2',
    front1: '#2A5CB2',
    back2: '#D9DCE2',
    back1: '#9CA6BA',
    strand2: '#D9DCE2',
    strand1: '#9CA6BA',
    
  }

  f1 = ({front1, front2, back1, back2, strand1, strand2}) => ({
   
    front1 : front2,
    front2: "#2A5CB2",
    back1: back2,
    back2: '#9CA6BA',
    strand1: strand2 ,
    strand2: '#9CA6BA',
    
  });

  f2 = ({front1, front2, back1, back2, strand1, strand2}) => ({
    front1 : front2,
    front2: "#84BB23",
    back1: back2,
    back2: '#2A5CB2',
    strand1: strand2 ,
    strand2: '#2A5CB2',
    
  });

  f3 = ({front1, front2, back1, back2, strand1, strand2}) => ({
    front1 : front2,
    front2: "#D9DCE2",
    back1: back2,
    back2: '#D9DCE2',
    strand1: strand2 ,
    strand2: '#D9DCE2',
    
  });
  


  componentDidMount =() =>{
    //  setTimeout(this.animate,1000);
    
    let self = this;
    let funArr = [self.f1, self.f2, self.f3];




    
    setInterval(() => {
      combination = k;
      k += 1;
      k = k % 3;
      self.setState(funArr[k]);
      
    }, 1000);
   


  }

  onClick =(e) =>{
    if (this.props.disabled) {
      e.preventDefault();
      return;
    }

    if (this.props.onClick) {
      this.props.onClick(e);
    }
  }

  render() {

    

    let {

      className,

      ...attributes
    } = this.props;
    let logo1;
    let logo2;
    logo1 = <Samy path="../logo1.svg">
<SvgProxy selector="#front2" stroke={this.state.front2} class={animate4} />
<SvgProxy selector="#back2" stroke={this.state.back2} class={animate4}/>
<SvgProxy selector="#strand2" stroke={this.state.strand2} class={animate2}/>  
<SvgProxy selector="#strands2" class={animate2}/>  
    </Samy>;

    if (combination===0){  

     logo2 = <Samy path="../logo2.svg">
<SvgProxy selector="#front2" stroke={this.state.front2} class={animate4} />
<SvgProxy selector="#back2" stroke={this.state.back2} class={animate4}/>
<SvgProxy selector="#strand2" stroke={this.state.strand2} class={animate2}/>  
<SvgProxy selector="#strands2" class={animate2}/>  
</Samy>;
}

else if (combination===1){  

 logo2 = <Samy path="../logo2.svg">
<SvgProxy selector="#front2" stroke={this.state.front2} class={animate4} />
<SvgProxy selector="#back2" stroke={this.state.back2} class={animate4}/>
<SvgProxy selector="#strand2" stroke={this.state.strand2} class={animate2}/>  
<SvgProxy selector="#strands2" class={animate2}/>  
</Samy>;
}

else if (combination===2){  

 logo2 = <Samy path="../logo2.svg">
<SvgProxy selector="#front2" stroke={this.state.front2} class={animate4} />
<SvgProxy selector="#back2" stroke={this.state.back2} class={animate4}/>
<SvgProxy selector="#strand2" stroke={this.state.strand2} class={animate2}/>  
<SvgProxy selector="#strands2" class={animate2}/>  
</Samy>;
}



    let classes = classNames(
      className
    );


      return ( <div { ...attributes} className = {classes} style = {{fontWeight : 600}} onClick = {this.onClick}>
      <div className={svgBlock}>
        <div className={svgContainer}>
    <Samy path="../logo1.svg">
    <SvgProxy selector="#front1" stroke={this.state.front1} class={animate3}/>
    <SvgProxy selector="#back1" stroke={this.state.back1} class={animate3}/>
    <SvgProxy selector="#strand1" stroke={this.state.strand1} class={animate1} />  
    <SvgProxy selector="#strands1" class={animate1}/>  
    </Samy>
</div>
<div className={svgContainer}>
<Samy path="../logo2.svg">
<SvgProxy selector="#front2" stroke={this.state.front2} class={animate4} />
<SvgProxy selector="#back2" stroke={this.state.back2} class={animate4}/>
<SvgProxy selector="#strand2" stroke={this.state.strand2} class={animate2}/>  
<SvgProxy selector="#strands2" class={animate2}/>  
</Samy>;
</div>

        </div>
        </div>
        
      );
    }
  }



Loader.propTypes = propTypes;
Loader.defaultProps = defaultProps;

export default Loader;

const seconds = '1s';

const strand_shrink = keyframes`

  0% {
    opacity: 1;
    stroke-dasharray: 0 20;
    stroke-dashoffset: -10;
  }
  100% {
    opacity: 1;
    stroke-dasharray: 20 20;
    stroke-dashoffset: 0;
  }
`
const strand_shrink_2 = keyframes`

  from {
    opacity: 1;
    stroke-dasharray: 0 20;
    stroke-dashoffset: -20;
  }
  to {
    opacity: 1;
    stroke-dasharray: 20 20;
    stroke-dashoffset: 0;
  }
`
const draw = keyframes`

  from {
    opacity: 1;
    stroke-dasharray: 150 150;
    stroke-dashoffset: 150;
  }
  to {
    opacity: 1;
    stroke-dasharray: 150 150;
    stroke-dashoffset: 0;
  }
`

const animate1 = css`

  animation: ${strand_shrink} ${seconds} ease forwards;
  
`

const animate2 = css`
  fill-opacity: 0;
  opacity: 0;
  animation: ${strand_shrink_2} ${seconds} ease;

  animation-iteration-count: infinite;
  
`

const animate3 = css`
  animation: ${draw} ${seconds} linear;
  
`

const animate4 = css`
  fill-opacity: 0;
  opacity: 0;
  animation: ${draw} ${seconds} linear forwards;

  animation-iteration-count: infinite;
  
`

const svgContainer = css` 
  position: absolute;
  top:0px;
  
`
const svgBlock = css` 

    position: relative;
    width: 137px;
  height: 76px;
  transform: scale(0.5, 0.5);
  
`
