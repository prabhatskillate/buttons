// import "./global-styles.less";

// .btn {
//     height: 34px;
//     padding: auto 16px;
// }


import * as theme from '../global-styles.js';
import { css } from 'react-emotion';

//import {fontColor, fontSize} from '../global-styles.js';

export const btn = css`

  display:flex;
  font-family: ${theme.fontFamily.text};
  height: ${theme.dimensions.buttonHeight};
  padding: 0 12px;
  border-radius : ${theme.radius.button};
  cursor: pointer;
  font-size: ${theme.fontSize.regular};
  font-weight: ${theme.fontWeight.bold};
  &:disabled{
    background-color: ${theme.backgroundColors.veryLightGrey};
  color:${theme.fontColor.veryLight};
  border: 1px solid ${theme.borderColors.default};
  cursor: not-allowed;
  &:hover {
    background-color: ${theme.backgroundColors.veryLightGrey};
    border: 1px solid ${theme.borderColors.default};
    color:${theme.fontColor.veryLight};
  }
  &:active {

    box-shadow: none;

  }
  }
`

export const btn_blue = css`
  background-color: ${theme.colors.defaultblue};
  color:${theme.fontColor.darkBackground};
  border: 1px solid ${theme.colors.defaultblue};
  border-bottom:${theme.borderColors.borderbottom};
  &:hover {
    background-color: ${theme.colors.hoverBlue};
    border: 1px solid ${theme.colors.hoverBlue};
    color:${theme.fontColor.white};
  }
  &:active {
    box-shadow: ${theme.shadow.active};
  }
`
export const btn_regular = css`
  background-color: initial;
  color:${theme.fontColor.light};
  border: 1px solid ${theme.borderColors.default};
  border-bottom: ${theme.borderColors.borderbottom};
  &:hover {
    background-color: initial;
    border: 1px solid ${theme.borderColors.active};
    color:${theme.fontColor.dark};
  }
  &:active {

    box-shadow: ${theme.shadow.active};

  }
`
export const btn_subtle = css`
  background-color: initial;
  color:${theme.fontColor.light};
  border:none;
  &:hover {
    background-color: ${theme.backgroundColors.lightGrey};
    color:${theme.fontColor.dark};
  }
  &:active {

    box-shadow: ${theme.shadow.active};

  }
`
export const btn_skeleton = css`
  background-color: initial;
  color:${theme.fontColor.darkBackground};
  border: 1px solid ${theme.borderColors.default};
  &:hover {
    color:${theme.fontColor.white};
    border: 1px solid ${theme.borderColors.white};
  }
  &:active {

    box-shadow: ${theme.shadow.active};

  }
`

export const disabled = css`
  background-color: ${theme.backgroundColors.veryLightGrey};
  color:${theme.fontColor.veryLight};
  border: 1px solid ${theme.borderColors.default};
  cursor: not-allowed;
  &:hover {
    background-color: ${theme.backgroundColors.veryLightGrey};
    border: 1px solid ${theme.borderColors.default};
    color:${theme.fontColor.veryLight};
  }
  &:active {

    box-shadow: none;

  }
`

export const red_text = css`
  color:${theme.colors.red};
  &:hover {
    color:${theme.colors.red};
  }
`

export const green_text = css`
  color:${theme.colors.green};
  &:hover {
    color:${theme.colors.green};
  }
`
export const alignContainer = css`
  display:flex;
  align-content: space-between;
  flex-grow: 1;
`

export const alignIcon = css`
  display:flex;
  align-items: center;
`

export const alignText = css`
  display:flex;
  align-items: center;
`
export const rowReverse = css`
  flex-direction:row-reverse;
`
export const buttonGrow = css`
  flex-grow:1;
`
export const paddingRight = css`
padding-right:8px;
`

export const anchor = css`

  text-decoration: none;
  display:flex;
  font-family: ${theme.fontFamily.text};
  cursor: pointer;
  font-size: ${theme.fontSize.regular};
  font-weight: ${theme.fontWeight.bold};
  color: ${theme.fontColor.primaryLink};
  &:hover {
  text-decoration: underline;
  }
`

export const anchorsecondary = css`
color: ${theme.fontColor.secondaryLink} ;

`
export const anchorexternal = css`
color: ${theme.fontColor.externalLink} ;

`
export const anchordisabled = css`
color:${theme.fontColor.veryLight};
cursor: not-allowed;

`

export const column_btn = css`
flex-grow: 1;
flex-direction: column;

`